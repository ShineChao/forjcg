var admin = require("firebase-admin");

var serviceAccount = require("../membermanagement-77d73-firebase-adminsdk-49vhj-0af583b1ef.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://membermanagement-77d73-default-rtdb.firebaseio.com"
});

var db = admin.database();
module.exports = db;
