var firebase      = require("firebase");

var firebaseConfig = {
  apiKey: "AIzaSyAUNtp9_D0OZDJZxjXDcWtEd3FOTM6w4JY",
  authDomain: "membermanagement-77d73.firebaseapp.com",
  databaseURL: "https://membermanagement-77d73-default-rtdb.firebaseio.com",
  projectId: "membermanagement-77d73",
  storageBucket: "membermanagement-77d73.appspot.com",
  messagingSenderId: "521035727262",
  appId: "1:521035727262:web:bfae85afef98d09542568f",
  measurementId: "G-X7MSMJQDL7"
};

// Initialize Firebase
var app = firebase.initializeApp(firebaseConfig);
module.exports = firebase;