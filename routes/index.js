var express = require('express');
var router = express.Router();
var firebaseDb = require('../connections/firebase_admin_connect');
var firebase = require('../connections/firebase_connect');

// 
router.get('/', function (req, res, next) {
    firebaseDb.ref('list').once('value',function(snapshot){
        var auth = req.session.uid;
        res.render('index', {
            title: '會員管理系統',
            auth: auth,
            error: req.flash('errors'),
            list:snapshot.val()
        });
    })

    // 測試有連到firebase.auth
    // console.log(firebase.auth());

    // 測試連到fireBase的Db，如連到 console.log"hello"
    // firebaseDb.ref().once('value',function(snapshot){
    //     console.log(snapshot.val());
    // })
});
/* GET home page. */
module.exports = router;