var express = require('express');
var router = express.Router();
var firebaseDb = require('../connections/firebase_admin_connect');
var firebase = require('../connections/firebase_connect');
var fireAuth = firebase.auth();

router.get('/', function (req, res) {
    res.render('login', { title: '登入' });
})
router.post('/', function (req, res) {
   fireAuth.signInWithEmailAndPassword(req.body.email,req.body.passwd)
   .then(function(user){
        //console.log("user111", user,typeof(user));
        //console.log("user.user.uid111", user.user.uid);
        req.session.uid = user.user.uid;
        //console.log("req.session.uid111", req.session.uid);
        res.redirect('/');
   })
   .catch(function(error){
        var errorMessage = error.message;
        console.log("errorMessage111",errorMessage)
       res.redirect('/')

   })
})
module.exports = router;