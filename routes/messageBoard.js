var express = require('express');
var router = express.Router();
var firebaseDb = require('../connections/firebase_admin_connect');

// 會員新增留言
router.post('/', function(req, res){
    firebaseDb.ref('user/'+req.session.uid).once('value',function(snapshot){
        var nickname = snapshot.val().nickname;
        var ref = firebaseDb.ref('list').push();
        var listContent = {
            nickname: nickname,
            content : req.body.content
        }
        ref.set(listContent)
        .then(function(){
            res.redirect('/')
        })
    })
})
module.exports = router