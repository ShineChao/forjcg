var express = require('express');
var router = express.Router();
var firebaseDb = require('../connections/firebase_admin_connect');
var firebase = require('../connections/firebase_connect');
var fireAuth = firebase.auth();

// 於註冊頁面顯示一次錯誤訊息
router.get('/', function (req, res) {
    res.render('signup', { title: '註冊', error: req.flash('error')});
})
router.post('/', function (req, res) {
    var email = req.body.email;
    var password = req.body.passwd;
    var nickname = req.body.nickname;
    // 儲存會員註冊資料
    firebase.auth().createUserWithEmailAndPassword(email, password)
    .then(function(user){
        console.log("user111",user);
        var saveUser = {
            'email':email,
            'password':password,
            'nickname':nickname,
            'uid':user.user.uid
        }

        firebaseDb.ref('/user/'+ user.user.uid).set(saveUser)
        res.redirect('/signup/success')
    })
    .catch(function(error){
        var errorMessage = error.message;
        console.log("errorMessage111",errorMessage)
        req.flash('error',errorMessage)
        res.redirect('/signup')
    })
})
router.get('/success',function(req,res){
    res.render('success',{
        title:'註冊成功'
    });
})
module.exports = router;